import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	val kotlinVersion = "1.3.21"
	id("org.springframework.boot") version "2.1.7.RELEASE"
	id("io.spring.dependency-management") version "1.0.7.RELEASE"
	kotlin("jvm") version "1.2.71"
	kotlin("plugin.spring") version "1.2.71"
	id("com.bmuschko.docker-spring-boot-application") version "5.0.0"
}

version = "1.0.0-SNAPSHOT"
extra["springCloudVersion"] = "Greenwich.SR2"

repositories {
	mavenCentral()
	jcenter()
}

dependencies {
	implementation("org.springframework.boot","spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-devtools") //Enables dev mode so we don't have to restart our app all the time
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.json:json:20090211")
	implementation ("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("mysql:mysql-connector-java")
	implementation("org.liquibase:liquibase-core")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
	languageVersion = "1.3"
}

docker {
	springBootApplication {
		baseImage.set("openjdk:8-alpine")
		ports.set(listOf(8082, 8082))
		tag.set("roboops/robo-ops:latest")
//		jvmArgs.set(listOf("-Dspring.profiles.active=production", "-Xmx2048m"))
	}
}
