Intro:
This tool is meant to auto-generate a CI/CD pipeline for you. 

HighLevel Design

![HighLevel Design](DevOps_As_a_service_HLD.png)

Pre-dev steps: 

```
docker run -d --name=dev-consul -p 8500:8500 -e CONSUL_BIND_INTERFACE=eth0 consul  
docker run -d --name=dev-jenkins -p 9080:8080 -p 50000:50000 jenkins/jenkins:lts
docker run -d -p 8081:8081 --name nexus sonatype/nexus3
```

toRun:
```
$ ./gradlew bootRun
```
If you don't have gradlew, download it from https://buildTools.gradle.org/install/


todo:
How to ensure the respective plugins extends e.g. sourceControlInterface

Info: Alot of the choices of tech to dev on is basically do they have a docker iamge I can run locally


Help:
```
git submodule update --init --recursive
```
