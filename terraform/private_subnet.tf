locals {
  vpc_id             = aws_vpc.roboops.id
  private_cidr_block  = ""
  count = "2"
}


resource "aws_subnet" "private" {
  count             = "2"
  vpc_id            = "${local.vpc_id}"
  availability_zone =  data.aws_availability_zones.available.names[count.index]
  cidr_block        = "10.0.${count.index + 2}.0/24"
  tags = map(
  "Name", "roboops_private ${data.aws_availability_zones.available.names[count.index]}"
  )

}

resource "aws_network_acl" "private" {
  count      = "${local.count}"
  vpc_id     = "${local.vpc_id}"
  subnet_ids = ["${aws_subnet.private[count.index].id}"]

  depends_on = ["aws_subnet.private"]
}

resource "aws_route_table" "private" {
  count  =  "${local.count}"
  vpc_id = "${local.vpc_id}"
}


resource "aws_route_table_association" "private" {
  count          =  "${local.count}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
  depends_on     = ["aws_subnet.private", "aws_route_table.private"]
}

resource "aws_route" "default" {
  count                  =  "${local.count}"
  route_table_id         = "${aws_route_table.private[count.index].id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = ""

  depends_on             = ["aws_route_table.private"]
}