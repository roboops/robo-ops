//Modification of the example of terraform template here: https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples/eks-getting-started

resource "aws_vpc" "roboops" {
  cidr_block = "10.0.0.0/16"

  tags = map(
  "Name", "roboops"
  )
}

resource "aws_subnet" "roboops_public" {
  count = 2

  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = "10.0.${count.index}.0/24"
  vpc_id            = aws_vpc.roboops.id

  tags = map(
  "Name", "roboops_public ${data.aws_availability_zones.available.names[count.index]}"
  )
}

resource "aws_internet_gateway" "roboops" {
  vpc_id = aws_vpc.roboops.id
  tags = {
    Name = "roboops"
  }
}

resource "aws_route_table" "roboops_public" {
  vpc_id = aws_vpc.roboops.id
  //Needs a name
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.roboops.id
  }
}

resource "aws_route_table_association" "roboops_public" {
  count = 2
  subnet_id      = aws_subnet.roboops_public.*.id[count.index]
  route_table_id = aws_route_table.roboops_public.id
}