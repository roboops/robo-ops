Tell me and I forget.\
Teach me and I remember.\
Involve me and I learn.\
    -(Benjamin Franklin)
    
    
TODO 
1) all these endpoints need to be moved into its own service to make it comply with
the mvc model. Or at least have the diff controller register under a different service
name


3) need to add loggingß
4) need to have tests
5) need a pipeline for this as well
7) need to fix WARNING: An illegal reflective access operation has occurred


Testing sequence:
1) http://localhost:90/language/set?language=kotlin
1) http://localhost:90/bt/gradle/gencmd
1) http://localhost:90/sc/github/createToken?username=xijerobot&password=p1234w1234?
1) http://localhost:90/sc/github/createrepo?reponame=robotesting
1) http://localhost:90/citools/jenkin/setURl?url=http://localhost:9080
1) http://localhost:90/citools/jenkin/createtoken?username=jenkinsAdmin&password=p1234w1234?
1) http://localhost:90/citools/jenkin/createjob
1) http://localhost:90/ct/docker/createDockerFile
1) http://localhost:90/repo/nexus/setURl?url=http://localhost:8081/
    
gvxxriC3diK22JLt