package hello.buildTools.gradle

import hello.genericHelpers.Status
import java.util.*

fun createBuildCommand(): Status {
    println("pete = ${get("sourceLanguage").status}")

    val sourceLanguage = if (get("sourceLanguage").status == "pass")
        get("sourceLanguage").value
    else return Status("fail", "SourceLanguage has not been set")


    var buildCmd = when (sourceLanguage) {
        "kotlin" -> kotlinbuildCmd
        "java" -> javabuildCmd
        else -> "LANGUAGE NOT DEFINED YET"
    }

    var deployCmd = when (sourceLanguage) {
        "kotlin" -> """ steps {
								sh './gradlew publish'
		}""".trimIndent()
        "java" -> """ steps {
								sh './gradlew publish'
		}""".trimIndent()
        else -> "LANGUAGE NOT DEFINED YET"
    }

    var buildFile = when (sourceLanguage) {
        "kotlin" -> "build.gradle.kts"
        "java" -> "build.gradle"
        else -> "LANGUAGE NOT DEFINED YET"
    }

    update("buildCmd", Base64.getEncoder().encodeToString(buildCmd.toByteArray()))
    update("deployCmd", Base64.getEncoder().encodeToString(deployCmd.toByteArray()))
    update("buildToolName", "gradle")
    update("builtFile", buildFile)
    return Status("Pass", "Build Cmd updated : $buildCmd")
}



val kotlinbuildCmd =
    """steps {
            script {
                    try {
                           sh './gradlew build' 
                    }finally {
                            echo 'somethign else'
            }}}""".trimIndent()



val javabuildCmd =
    """steps {
            script {
                    try {
                           sh './gradlew build' 
                    }finally {
                            echo 'somethign else'
            }}}""".trimIndent()
