package hello.buildTools.gradle

import hello.genericHelpers.Status
import org.springframework.web.client.RestTemplate



fun update(field: String, value : String ): Status {
    val restTemplate = RestTemplate()
    return restTemplate.getForObject("http://localhost:8082/db/update?field=$field&value=$value", Status::class.java)
}

fun updateBoolean(field: String, value : String ): Status {
    val restTemplate = RestTemplate()
    return restTemplate.getForObject("http://localhost:8082/db/updateBoolean?field=$field&value=$value", Status::class.java)
}

fun get(field: String): Status {
    val restTemplate = RestTemplate()
    return restTemplate.getForObject("http://localhost:8082/db/get?field=$field", Status::class.java)
}