package hello.language

import org.springframework.web.bind.annotation.*
import hello.genericHelpers.Status


@RestController
@RequestMapping("/language")
class LanguageController{

	@GetMapping("/")
	fun hello() = "Commands available are "


	@GetMapping("/health")
	fun health() =
            Status("Pass", "Its ok.....?") //Still not a json, how the hell do you create a JSON??


	@GetMapping("/set")
		fun authenticateWithTokenInstance(@RequestParam("language", required = true, defaultValue = "kotlin") language: String) : Status
			= update("sourceLanguage", language.toLowerCase())

}

