package hello.container.docker


import hello.genericHelpers.Status
import org.springframework.web.client.RestTemplate
import java.io.File
import java.time.LocalDateTime
import java.util.*


fun createDockerFile() : Status {
    val file = File("src/main/resources/dockerfileTemplate").readText()
    val base64EncodedString = Base64.getEncoder().encode((file).toByteArray()).toString(Charsets.UTF_8)

    return uploadFile("dockerfile", base64EncodedString, "Created via robo-ops ${LocalDateTime.now()}", true)
}

fun uploadFile(filename: String, content: String, commitmsg: String, overwrite: Boolean): Status{
    val restTemplate = RestTemplate()
    return restTemplate.getForObject("http://localhost:90/sc/github/uploadFile?filename=$filename&content=$content&commitmsg=$commitmsg&overwrite=$overwrite", Status::class.java)
}