package hello.container.docker

import org.springframework.web.bind.annotation.*
import hello.genericHelpers.Status

@RestController
@RequestMapping("/ct/docker")
class dockerController{
	@GetMapping("")
	fun hello() = "Commands available are /createDockerFile"

	@GetMapping("/createDockerFile")
	fun health() : Status = createDockerFile()
}

