package hello.database

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.queryForObject
import org.springframework.stereotype.Service



/*
TODO needs to format the return to status and also put error checking on it,
Need to handle the fact that projectname is unique
 */

@Service
class dbHelper  {

    @Autowired
    private val jdbcTemplate: JdbcTemplate? = null

    var allUsers: Int? = null
        get() = jdbcTemplate!!.queryForObject("select count(1) from roboops_table", Int::class.java)

    fun create(name: String) {
        jdbcTemplate!!.update("insert into roboops_table (projectname)  values(?)", name)
    }

    fun update( field: String, value: String) {
        var dave = value.removeSurrounding("\"")
        jdbcTemplate!!.update("update roboops_table set $field = '$dave' Where id = '4'")
    }

    fun updateBoolean( field: String, value: Boolean) {
        jdbcTemplate!!.update("update roboops_table set $field = $value Where id = '4'")
    }

    fun get(field: String) : String {
        return jdbcTemplate!!.queryForObject("select $field from roboops_table where id = '4'", String::class.java)
    }

    fun deleteByName(id: Int) {
        jdbcTemplate!!.update("delete from roboops_table where id = ?", id)
    }
}