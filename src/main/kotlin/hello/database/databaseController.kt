package hello.database

import org.springframework.web.bind.annotation.*
import hello.genericHelpers.Status
import org.springframework.beans.factory.annotation.Autowired

@RestController
@RequestMapping("/db")
class databaseController{

	@GetMapping("/")
	fun hello() = "Commands available are "


	@GetMapping("/health")
	fun health() =
            Status("pass", "Its ok.....?") //Still not a json, how the hell do you create a JSON??


	@Autowired
	val service = dbHelper()

	@GetMapping("/create")
	fun create(@RequestParam("pn", required = true) projectName: String) : Status {
		service.create(projectName)
		return Status("pass", "hello")
	}

	@GetMapping("/update")
	fun updaterun(@RequestParam("field", required = true) field: String,
				  @RequestParam("value", required = true) value: String) : Status {
		service.update(field,value)
		return Status("pass", "Update column $field with value $value")
	}

	@GetMapping("/updateBoolean")
	fun updateBoolean(@RequestParam("field", required = true) field: String,
				  @RequestParam("value", required = true) value: Boolean) : Status {
		service.updateBoolean(field,value)
		return Status("pass", "Update column $field with value $value")
	}

	@GetMapping("/get")
	fun getE(@RequestParam("field", required = true) field: String) : Status {
		return Status("pass", "${service.get(field)}")
	}

}

