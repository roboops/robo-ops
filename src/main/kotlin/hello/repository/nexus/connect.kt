package hello.repository.nexus


import hello.genericHelpers.Status

fun connect () : Status {

    val nexusURL = get("nexusURL").value


    return Status("pass", "url exists = ${checkIfURLExists(nexusURL)}")
}