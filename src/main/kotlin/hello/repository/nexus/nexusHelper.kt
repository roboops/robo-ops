package hello.repository.nexus

import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate

fun checkIfURLExists(url: String): Boolean{
    try {
        val restTemplate = RestTemplate()
        restTemplate.headForHeaders(url)
        return true
    } catch (e: RestClientException) {
        return false
    }
}