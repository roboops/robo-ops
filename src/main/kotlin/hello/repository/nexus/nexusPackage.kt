package hello.repository.nexus

import hello.genericHelpers.Status

fun packageNexus():Status{
    //For file add additional parameters
    downloadBuildFile()
    return Status("pass", "pete")
}

fun downloadBuildFile():Status{

    val builtFile = if (get("builtFile").status == "pass")
        get("builtFile").value
    else return Status ("fail", "build file name has not been set")


                println("download $builtFile")

    //download file
    return Status("pass", "downloadig $builtFile")

}