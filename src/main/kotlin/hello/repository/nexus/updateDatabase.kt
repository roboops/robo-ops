package hello.repository.nexus

import hello.genericHelpers.Status
import org.springframework.web.client.RestTemplate



fun update(field: String, name : String): Status {
    val restTemplate = RestTemplate()
    return restTemplate.getForObject("http://localhost:8082/db/update?field=$field&value=$name", Status::class.java)
}

fun get(field: String): Status {
    val restTemplate = RestTemplate()
    return restTemplate.getForObject("http://localhost:8082/db/get?field=$field", Status::class.java)
}