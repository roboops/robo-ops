package hello.repository.nexus

import org.springframework.web.bind.annotation.*
import hello.genericHelpers.Status

const val gitHubAPI_URL = "https://api.github.com" //TODO will this ever change?

//TODO need to look at securing our rest calls to github
@RestController
@RequestMapping("/repo/nexus")
class NexusController{

	@GetMapping("")
	fun hello() = "Commands available are /connect /setURL /createrepo /uploadFile"

	@GetMapping("/health")
	fun health() =
			Status("Pass", "Its ok.....?")

	@GetMapping("/connect")
	fun connectInstance(): Status = connect()

	@GetMapping("/setURL")
	fun setNexusURL(@RequestParam("url", required = true) url: String) : Status =
			update("repositoryURL", url)

	@GetMapping("/package")
	fun packageNexusInstance() : Status =
			packageNexus()

}

