package hello.genericHelpers

import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestTemplate

/*
TODO We may have todo multuple of these, as the failonempty=false shouldnt be a default
 */
object RestTemplateCreator {
     fun getRestTemplates(): RestTemplate {
         return RestTemplateBuilder()
                 .errorHandler(RestTemplateResponseErrorHandler())
                 .build()
    }

    fun getRestTemplatesWithoutFailOnEmptyBeans(): RestTemplate {
        val restTemplate = RestTemplateBuilder()
                .errorHandler(RestTemplateResponseErrorHandler())
                .build()

        val jsonHttpMessageConverter = MappingJackson2HttpMessageConverter()
        jsonHttpMessageConverter.objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)  //Good for when non-serializable stuff is returned
        restTemplate.messageConverters.add(jsonHttpMessageConverter)
        return restTemplate
    }

    /*
    Thinking breaking up the error handler, as we use more plugins and having to handle their diff error codes it's no longer feasible to use one
     */
    fun getRestTemplatesWithCustomErrorHandler(errorHandlerInstance: ResponseErrorHandler): RestTemplate {
        return RestTemplateBuilder()
                .errorHandler(errorHandlerInstance)
                .build()
    }

}