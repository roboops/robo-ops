package hello.genericHelpers

import org.springframework.http.client.ClientHttpResponse
import org.springframework.stereotype.Component
import java.io.IOException
import org.springframework.web.client.ResponseErrorHandler


@Component
class RestTemplateResponseErrorHandler : ResponseErrorHandler {

    @Throws(IOException::class)
    override fun hasError(httpResponse: ClientHttpResponse): Boolean {
        return false //everything is false so that it gets handle by our response handler
    }


    @Throws(IOException::class)
    override fun handleError(httpResponse: ClientHttpResponse) {
        println(httpResponse)
    }



}