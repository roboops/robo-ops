package hello.genericHelpers

data class Status(
        val status: String, //todo Should convert this to org.springframework.boot.actuate.health.Status
        val value: String ="",
        val additionalInfo: String ="") //Setting default values, so they become optional arguments
