package hello.genericHelpers

import org.springframework.http.HttpHeaders
import java.util.*

object AuthHeaderCreators {
    fun createBasicauthHeaders(username: String, password: String): HttpHeaders {
        val auth = Base64.getEncoder().encode(("$username:$password").toByteArray()).toString(Charsets.UTF_8)
        val returnNewHTTPHeaders = HttpHeaders()
        returnNewHTTPHeaders.set("Authorization", "Basic $auth")
        return returnNewHTTPHeaders
    }

    fun authHeaderWithToken(token: String): HttpHeaders {
        val newHTTPHeaders = HttpHeaders()
        newHTTPHeaders.set("Authorization", "token $token")
        return newHTTPHeaders
    }
}