package hello

import hello.genericHelpers.Status
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.context.annotation.Configuration


@Configuration
@EnableAutoConfiguration
@RestController
class Gateway {
	@GetMapping("/health")
	fun health() =
			Status("pass", "Its ok.....?")

	@GetMapping("/error")
	fun error() =
			Status("fail", "Must try harder peter son.....?")


	@GetMapping("")
	fun hello() =
			Status("pass", "Hello")
}