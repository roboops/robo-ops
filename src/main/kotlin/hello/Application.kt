package hello

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Configuration
import org.springframework.util.AntPathMatcher
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@SpringBootApplication
class Application
fun main(args: Array<String>) {
	SpringApplication.run(Application::class.java, *args)
}

//Remove case sensitive urls
@Configuration
class WebConfig : WebMvcConfigurer {
	override fun configurePathMatch(configurer: PathMatchConfigurer) {
		val matcher = AntPathMatcher()
		matcher.setCaseSensitive(false)
		configurer.pathMatcher = matcher
	}
}