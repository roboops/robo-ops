package hello.sourceControl

/*

Authentication should give you something like this, use postman to help you; Your body should have
{
  "scopes": [
    "public_repo"
  ],a
  "note": "admin script"
}

response:

{
    "id": 310737223,
    "url": "https://api.github.com/authorizations/310737223",
    "app": {
        "name": "admin script",
        "url": "https://developer.github.com/v3/oauth_authorizations/",
        "client_id": "00000000000000000000"
    },
    "token": "3badddd624989f95a95a2b1894dcfcfd533517b1",
    "hashed_token": "fd6311668d4daa3238c9947d0fea5057c404c98628e2d21d41a0bcc2dc549bb3",
    "token_last_eight": "533517b1",
    "note": "admin script",
    "note_url": null,
    "created_at": "2019-07-22T15:16:33Z",
    "updated_at": "2019-07-22T15:16:33Z",
    "scopes": [
        "public_repo"
    ],
    "fingerprint": null
}

 */