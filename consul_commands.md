To add an extra consul host to test the federation
```
docker run -d -e CONSUL_BIND_INTERFACE=eth0 consul agent -dev -join=172.17.0.2
```

Rerunning docker instance
```$xslt
docker run -d -p 8500:8500 -e CONSUL_BIND_INTERFACE=eth0 consul //may need to restart docker, it be funkuy
```

this will setup consul where we will store all our data and also act as service discovery

test to see consul working

```
docker exec -t dev-consul consul members
```

to health check consul locally e.g. we can send date to our consul instance
```$xslt
curl http://localhost:8500/v1/health/service/consul?pretty
```

readme: https://hub.docker.com/_/consul


to get data
```curl localhost:8500/v1/kv/susan``` Remember the data coming back is a in base64 value

to put data
```curl -X PUT http://localhost:8500/v1/kv/dave -H 'Content-Type: application/json' -d 'petere oete'```